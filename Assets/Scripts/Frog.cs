using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : MonoBehaviour
{
    [SerializeField] private float leftCap;
    [SerializeField] private float rightCap;
    [SerializeField] private float jumpLength = 2;
    [SerializeField] private float jumpHeight = 5;
    [SerializeField] private LayerMask Ground;


    private Collider2D coll;
    private bool facingLeft = true;
    private Rigidbody2D rb;
    private Animator anim;

    private void Start()
    {
        coll = GetComponent<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (facingLeft)
        {
            //ensures sprite is facing the correct way

            if (transform.position.x > leftCap)
            {

                if (transform.localScale.x != 1)
                {
                    transform.localScale = new Vector3(1, 1);
                }

                //test to see if frog is on ground, if so jump
                if (coll.IsTouchingLayers(Ground))
                {
                    rb.velocity = new Vector2(-jumpLength, jumpHeight);
                }
            }
            else
            {
                facingLeft = false;
            }

            //if it is not greater then frog will face right
        }

        else
        {


            //check if the x value is greater than the leftCap

            //ensures sprite is facing the correct way


            if (transform.position.x < rightCap)
            {
                //test to see if frog is on ground, if so jump
                if (transform.localScale.x != -1)
                {
                    transform.localScale = new Vector3(-1, 1);
                }


                if (coll.IsTouchingLayers(Ground))
                {
                    rb.velocity = new Vector2(jumpLength, jumpHeight);

                }
            }

            else
            {
                facingLeft = true;
            }




        }

    }
    public void JumpedOn()
    {
        anim.SetTrigger("Death");
    }

    public void Death()
    {
        Destroy(this.gameObject);
    }
}
